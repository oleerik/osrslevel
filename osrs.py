import urllib3
import csv

skills = ["Overall","Attack","Defence","Strength","Constitution","Ranged","Prayer","Magic","Cooking","Woodcutting","Fletching","Fishing","Firemaking","Crafting","Smithing","Mining","Herblore","Agility","Thieving","Slayer","Farming","Runecrafting","Hunter","Construction"];


def _fetchhiscores(name):
    http = urllib3.PoolManager()
    url = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=%s" % (name)
    request = http.request("GET", url).data.decode("UTF-8").splitlines()
    return request

def _makedict(name):
    levels=[]
    cr =  csv.reader(_fetchhiscores(name))
    for row in cr:
        levels.append(row[1])
    dictA = dict(zip(skills, levels))
    return dictA

#Function to get singular skill level
def level(name,skill):
    print(skill)
    
#Function to get all skills and levels
def allskills(name):
    print("")

print(_makedict("Ledria Nox"))
